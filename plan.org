

* Pourquoi seriez-vous intéressé par GNU Guix [5min]
** Scenarii

   + Alice utilise foo@1.2, bar@3.4
   + Carole collabore avec Alice...
     mais utilise foo@4.5 et bar@7.8 pour un autre projet
   + Charlie mets à jour son système et tout est cassé
   + Bob utilise les mêmes versions qu'Alice mais n'a pas le même résultat
   + Dan essaie de rejouer plus tard le scénario d'Alice
     mais rencontre l'enfer des dépendances

** Solution(s)

   1. gestionnaire de paquets : APT, YUM, etc.
   2. gestionnaire d'environnements : Modulefiles, Conda, etc.
   3. conteneur : Docker, Singularity

   Guix = #1 + #2 + #3

** Concrètement

   Soit vous vous reconnaissez dans un scénario,
   Soit vous n'êtes pas satisfait par une des solutions,
   Soit vous êtes curieux d'un nouveau outil.

   Dans les 3 cas, ce tutoriel explique:

   + Quel est le problème à traiter ?  Et pourquoi c'est compliqué.
   + Comment faire en pratique avec GNU Guix.

   Ce tutoriel est complémentaire de la présentation de Julien Lepillier en
   2019 ([[https://replay.jres.org/videos/watch/c77b3a44-b75f-4c10-9f39-8fb55ae096d7][video]] et [[https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiN-YyZwLPzAhVGzRoKHdp3Aa4QFnoECBQQAQ&url=https%3A%2F%2Fconf-ng.jres.org%2F2019%2Fdocument_revision_5343.html%3Fdownload&usg=AOvVaw2t8m76qM9coQz7YLO5k8qY][article]])

* Pourquoi est-ce difficile ? [8min]
** Que signifie exécuter un programme

   Exemple avec Python, R, ou n'importe quel programme

   which foo
   ldd chemin/vers/foo

   But: expliquer l'analogie: Yahourt = Lait + Skyr*

   *Skyr: autre yaourt

   => Les sources ne suffisent pas pour la Reproductibilité

** Qu'est-ce qu'un gestionnaire de paquets

   gestionnaire des paquets = gestion des dépendances (directes)

   foo <- source@1.2 + executable@A + env@B
   bar <- source@1.2 + executable@C + env@D

   Est-ce que foo == bar ?

   Trop de combinaison pour avoir le même environnement sur plusieurs machines
   à différent point dans temps.

*** Exemple concret: Debian stable

    Donner deux machines utilisant Debian stable mais n'ayant pas le même environnement

** Comment capturer un environnement

   Conteneur = smoothie

   Conteneur : Dockerfile

   Mais comment sont construits les binaires auquel le Dockerfile réfère.
   Dans un Dockerfile, il y a un gestionnaire de paquets sous-jacent donc le
   problème est déplacé.

   À partir d'une image généré avec un Dockerfile à temps X, comment re-générer
   l'image à temps Y.

* Installation de GNU Guix [2min]
** Foreign distro
   guix-install.sh
   Le reste du tutoriel sera dans ce cadre
** Comme système d'exploitation

   Uniquement mention pour aller plus loin

* Gestion de paquets [30min]
  Lier avec les scenarii d'Alice, Carole, etc.
** guix package
*** impératif
    guix install foo
    guix install bar
*** declaratif
    déclaratif = fichier de configuration
    manifest.scm
*** profile
    plusieurs versions coexistent
*** environnement
    étendre temporaire un profil

    Bonus: --pure et -C

    WARNING: "guix shell" remplace "guix environment"

** pack
   image Docker
   docker load ou docker push / pull
   docker run

** pull et rollback
   rollback des profiles
   rollback du pull

** describe
   fixer un état
   channels.scm

** time-machine
   restaurer un profile dans un état

** Pour aller plus loin [2min]
*** guix gc
    /gnu/store = cache partagé
*** guix import
*** variant
*** transformation

* Création d'images [15min]
** guix system
* Limitations

  + Linux
  + environnement isolé implique une forte transparence, i.e., difficile avec
    des parties propriétaires
