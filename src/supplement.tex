\mode*        %Follow up of Beamer configuration when using ignorenonframetext.

\clearpage
\appendix
\section{Bonus}

\subsection{Définition fonctionnelle d'un paquet}

Pour Guix, la construction d'un paquet est vue comme une fonction pure :

\begin{itemize}
\item la construction retournée est la même pour les mêmes arguments,
\item l'évaluation n'a pas d'effets de bord.
\end{itemize}
Par exemple, si la compilation sauvegarde la date, alors la
construction retournée n'est pas identiquement la même pour les mêmes
arguments.  Par ailleurs, l'évaluation a un effet de bord car fournir
une date est une action qui a besoin d'éléments extérieurs qui ne
dépendent pas uniquement ni exclusivement du corps de la fonction.


\thisslide{def-fonc}
\begin{frame}<presentation>[label=def-fonc, fragile, noframenumbering]{Définition fonctionnelle (\emph{derivation})
    \hfill \small{(A valeur d’illustration pour fixer les idées)}\normalsize}
  \vspace{-0.35cm}
  \begin{Verbatim}[commandchars=\\\{\}]
{def} pkg(\blue{source}, \magenta{build_sytem}, \magenta{arguments}, \violet{inputs}):
    src = fetch(source)
    {import} build_system {as} bs
    build = bs.builder(arguments)
    envBuild = build.add(inputs)
    binary = envBuild(src)
    {return} binary
  \end{Verbatim}

\begin{center}
   \texttt{python3 = pkg(\blue{Python}, \magenta{configure\_make},
     \violet{deps}=[bzip2, expat, ...]) }
\end{center}

  \vspace{-0.35cm}
   \begin{exampleblock}{}
    \begin{itemize}
    \item Les {sources} \texttt{src} sont obtenues à partir de la définition du
      paquet via \blue{\texttt{source}}
    \item La recette \texttt{build} est déduite de la définition du paquet via
      \magenta{\texttt{build-system}} et \magenta{\texttt{arguments}}
      (\texttt{./configure \&\& make \&\& make install})
    \item L’environnement \texttt{envBuild} est \uline{\textbf{totalement
          isolé}} et ne contient que le nécessaire défini par
      \magenta{\texttt{build-system}} étendus de dépendances
      \violet{\texttt{inputs}} supplémentaires
    \end{itemize}
   \end{exampleblock}
\end{frame}


\clearpage
\subsection{Autour du graphe des dépendances}

Suivant la transformation de paquets que nous appliquons, il peut arriver que
le \emph{monde} soit recompilé.  Ici, nous illustrons la signification d'un
contrôle fin du graphe des dépendances.

\bigskip

Nous commençons par construire la paquet \texttt{python} avec le chaîne de
compilation \texttt{gcc@7} via un fichier déclaratif \emph{manifeste}.  En
étant attentifs, nous remarquons que le paquet \texttt{ghc-emojis} sera
reconstruit.

\thisslide{around-dag}
\begin{frame}<presentation>[label=around-dag, fragile, noframenumbering]%
  {Transformations : pourquoi tout peut-il être recompilé ?}
  \begin{exampleblock}{}
\begin{verbatim}
$ guix build -m example/some-python-with-gcc7.scm --derivations --dry-run
...
/gnu/store/vi2j8aakp7jqxq8m97xvqk0d8q8i27s2-ghc-emojis-0.1.2.drv
...
\end{verbatim}
  \end{exampleblock}

  La recette définit :
  \begin{itemize}
  \item un \blue{code source} et potentiellement des modifications \emph{ad-hoc} (\verb+patch+)
  \item des \magenta{outils de construction} (compilateurs, moteur de
    production, etc., p. ex. \verb+gcc+, \verb+cmake+)
  \item des \violet{dépendances}
  \end{itemize}


  \begin{exampleblock}{}
    \begin{center}
      Pourquoi doit-on recompiler une bibliothèque Haskell sur les caractères
      emoji ?
    \end{center}
  \end{exampleblock}
\end{frame}

La question est pourquoi une pile logicielle Python à caractère scientifique
dépend-elle d'une bibliothèque Haskell de caractère emojis ?

\bigskip

Rappelons que Guix permet d'inspecter le graphe des dépendances.  Par exemple,
le paquet \texttt{python} a 137 dépendances (n\oe uds), majoritairement
indirectes.

\bigskip % because issue with layout


\thisslide{dag-bis}
\begin{frame}<presentation>[label=dag-bis, fragile, noframenumbering]%
  {Graph Acyclique Dirigé (\emph{DAG})}
  \vspace{-0.5cm}
  \begin{center}
    \texttt{guix graph -{}-max-depth=6 python | dot -Tpng > graph-python.png}
  \end{center}

  \vfill{}
  \includegraphics[width=\textwidth]{static/graph-python.png}
  \vfill{}

  Graphe complet : \red{Python = 137 n\oe uds}, Numpy = 189, Matplotlib = 915, Scipy
  = 1439 n\oe uds
\end{frame}



\thisslide{inspect-dag}
\begin{frame}<presentation>[label=inspect-dag, fragile, noframenumbering]%
  {Inspection du graphe des dépendances}
  \vspace{-0.2cm}
\begin{verbatim}
python-scipy The Scipy library provides efficient numerical routines
ghc-emojis   Conversion between emoji characters and their names
\end{verbatim}

  \vspace{-0.1cm}
  \begin{exampleblock}{}
    \begin{center}
      Quelle dépendance entre une bibliothèque scientifique et une
      bibliothèque de pictogrammes ?
    \end{center}
  \end{exampleblock}

  \begin{tabular}{p{0.5\linewidth}p{0.5\linewidth}}
    \begin{minipage}[t]{0.5\linewidth}
\begin{Verbatim}[commandchars=\\\{\}]
$ guix graph      \textbackslash
   --path \blue{python-scipy} \red{ghc-emojis}
\blue{python-scipy@1.7.3}
python-pydata-sphinx-theme@0.6.3
python-jupyter-sphinx@0.3.2
python-nbconvert@6.0.7
pandoc@2.14.0.3
\red{ghc-emojis@0.1.2}
\end{Verbatim}
    \end{minipage}
    &
      \begin{minipage}[t]{0.5\linewidth}
\begin{Verbatim}[commandchars=\\\{\}]
$ guix graph      \textbackslash
   -t bag-emerged \textbackslash
   --path \red{ghc-emojis} \violet{python}
\red{ghc-emojis@0.1.2}
ghc@8.10.7
\violet{python@3.9.9}
\end{Verbatim}
    \end{minipage}
  \end{tabular}

  \begin{alertblock}{}
    \begin{center}
      Guix permet un contrôle fin de l'arbre des dépendances et son inspection
    \end{center}
  \end{alertblock}
\end{frame}

Nous cherchons donc quel est le chemin entre la bibliothèque scientifique
Python Scipy et la bibliothèque Haskell Emojis.  Incroyable, non ?  Pour
construire Scipy, une bibliothèque Python traitant principalement de calcul
scientifique comme des solveurs de système linéaire, il faut donc un
compilateur Haskell comme GHC.

\medskip

Nous cherchons ensuite quel est le chemin entre la bibliothèque Haskell Emojis
et l'interpréteur Python.  Incroyable, à nouveau, car il y en a un.  Le
compilateur Haskell a besoin de Python.

\bigskip

Finalement, recompiler le paquet \texttt{python} va automatiquement recompiler
\texttt{ghc}.  Puis, comme le \emph{build system} du paquet
\texttt{ghc-emojis} n'est pas identiquement le même, ce paquet va être
reconstruit.  Et ainsi de suite\dots

\clearpage
\subsection{Pour fixer les idées}


\thisslide{exec}
\begin{frame}<presentation>[label=exec, fragile, noframenumbering]%
  {Que signifie exécuter un programe ?}
  \vspace{-0.5cm}
\begin{verbatim}
python3 -c 'print(1+2)'
\end{verbatim}

  \begin{itemize}
  \item \texttt{python3} est ici un exécutable binaire (lisible par une machine)
  \item Le code source de \emph{Python} est \href{https://github.com/python/cpython}{accessible} (lisible par une personne)
  \item \texttt{python3} dépend à l’exécution de bibliothèques (dépendances)
  \end{itemize}

  \begin{center}
    Comment \texttt{python3} est-il obtenu ?
  \end{center}

  Analogie: \texttt{yahourt} = \emph{Lait} + \texttt{skyr}
  (Skyr : Yaourt islandais)

  \begin{center}
    \texttt{python3} = \emph{Python} + \texttt{compilateur} (+ \texttt{make} + etc.)
  \end{center}

  \begin{alertblock}{Conclusion}
    \begin{itemize}
    \item Les sources (\emph{Python}) ne suffisent pas pour la Reproductibilité,
    \item il faut aussi connaître deux environnements :
      \begin{itemize}
      \item de construction (\texttt{compilateur}, \texttt{make}, etc.),
      \item d’exécution (dépendances)
      \end{itemize}
    \end{itemize}
  \end{alertblock}
\end{frame}

Pour trouver les dépendances nécessaires à l'exécution, il suffit de lancer
\texttt{ldd \$(which python3)}.  À titre d'illustration, ces bibliothèques sont
nécessaires~: \texttt{linux-vdso.so.1 libpthread.so.0 libutil.so.1 libm.so.6 ...}

\medskip

Par conséquent, on peut se demander :
\begin{enumerate}
\item Comment le binaire \texttt{python3} a-t-il été produit ?  Quel
  compilateur ?  Quelles dépendances ?
\item Comment les dépendances ont-elles été produites ?
\end{enumerate}


\thisslide{pkg-manager}
\begin{frame}<presentation>[label=pkg-manager, fragile, noframenumbering]%
  {Qu'est-ce qu'un gestionnaire de paquets ?}
  \begin{center}
    gestionnaire des paquets = gestion des dépendances
  \end{center}
  fournit dans une mesure l’environnement de construction et/ou d’exécution.

  \begin{exampleblock}{Est-ce que \texttt{foo} = \texttt{bar} ?}
    \begin{itemize}
    \item \texttt{foo} <- \emph{Source@1.2} + \texttt{env-construction@A}
    \item \texttt{bar} <- \emph{Source@1.2} + \texttt{env-construction@B}
    \end{itemize}
  \end{exampleblock}

  \begin{center}
    Comment s’assurer que deux environnements sont identiques ?
  \end{center}

  \begin{alertblock}{Conclusion}
    Trop de combinaisons pour avoir le même environnement :
    \begin{itemize}
    \item sur plusieurs machines,
    \item à différents points dans le temps.
    \end{itemize}
  \end{alertblock}
\end{frame}

